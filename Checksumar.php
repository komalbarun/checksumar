<?php 

namespace Checksumar;

class Checksumar
{
	public function __construct()
	{
		// Keep in case is needed later.
	}

	public function readBinaryFile(string $filepath, int $chunkbytes=8096)
	{
		$size = filesize($filepath);

		$chunk = 100;

		$ff = file_get_contents($filepath);

		$handle = fopen($filepath, 'rb');
		$f = "";

		while ($chunk <= $size) 
		{
			$chunk += $chunkbytes;
			$data = fread($handle, $chunk);

			yield $data;
		}

		fclose($handle);
	}

	public function getMd5Hash(string $filepath, int $chunkbytes=8096)
	{
		$all_data = "";

		foreach($this->readBinaryFile($filepath, $chunkbytes) as $data)
		{
			$all_data .= $data;
		}

		return md5($all_data);
	}

}